create or replace NONEDITIONABLE PACKAGE BODY DOHVAT AS
e_iznimka exception;
------------------------------------------------------------------------------------
--login
 PROCEDURE p_login(in_json in json_object_t, out_json out json_object_t )AS
    l_obj        json_object_t := json_object_t();
    l_input      VARCHAR2(4000);
    l_record     VARCHAR2(4000);
    l_username   korisnici.email%TYPE;
    l_password   korisnici.password%TYPE;
    l_id         korisnici.id%TYPE;
    l_out        json_array_t := json_array_t('[]');
 BEGIN
    l_obj.put('h_message', '');
    --l_obj.put('h_errcode', '');
    l_input := in_json.to_string;
    SELECT
        JSON_VALUE(l_input, '$.username' RETURNING VARCHAR2),
        JSON_VALUE(l_input, '$.password' RETURNING VARCHAR2)
    INTO
        l_username,
        l_password
    FROM
        dual;

    IF (l_username IS NULL OR l_password is NULL) THEN
       l_obj.put('h_message', 'Molimo unesite korisni�ko ime i zaporku');
       l_obj.put('h_errcod', 1);
       RAISE e_iznimka;
    ELSE
       BEGIN
          SELECT
             id
          INTO 
             l_id
          FROM
             korisnici
          WHERE
             email = l_username AND 
             password = l_password;
       EXCEPTION
             WHEN no_data_found THEN
                l_obj.put('h_message', 'Nepoznato korisnicko ime ili zaporka');
                l_obj.put('h_errcod', 2);
                RAISE e_iznimka;
             WHEN OTHERS THEN
                RAISE;
       END;

       SELECT
          JSON_OBJECT( 
             'ID' VALUE kor.id, 
             'Ime' VALUE kor.ime, 
             'Prezime' VALUE kor.prezime, 
             'Dat_rod' VALUE kor.dat_rod, 
             'Pozivni_id' VALUE kor.pozivni_id, 
             'Mobilni_broj' VALUE kor.mobilni_broj, 
             'Drzava_id' VALUE kor.drzava_id,
             'Created_at' VALUE kor.created_at,
             'Updated_at' VALUE kor.updated_at)
       INTO 
          l_record
       FROM
          korisnici kor
       WHERE
          id = l_id;

    END IF;

    l_out.append(json_object_t(l_record));
    l_obj.put('data', l_out);
    out_json := l_obj;
 EXCEPTION
    WHEN e_iznimka THEN
       out_json := l_obj; 
    WHEN OTHERS THEN
       --common.p_errlog('p_users_upd', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_input);
       l_obj.put('h_message', 'Gre�ka u obradi podataka');
       l_obj.put('h_errcode', 99);
       ROLLBACK;
 END p_login;


------------------------------------------------------------------------------------
--p_get_drzave 
procedure p_get_drzave(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
  l_obj JSON_OBJECT_T;
  l_drzave json_array_t :=JSON_ARRAY_T('[]');
  l_count number;
  l_id number;
  l_string varchar2(1000);
  l_search varchar2(100);
  l_page number; 
  l_perpage number; 

  BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.search'),
        JSON_VALUE(l_string, '$.page' ),
        JSON_VALUE(l_string, '$.perpage' )
    INTO
        l_id,
        l_search,
        l_page,
        l_perpage
    FROM 
       dual;

    FOR x IN (
            SELECT 
               json_object('ID' VALUE ID, 
                           'NAZIV' VALUE NAZIV,
                           'CREATED_AT' VALUE CREATED_AT,
                           'UPDATED_AT' VALUE UPDATED_AT) as izlaz
             FROM
                drzave
             where
                ID = nvl(l_id, ID) 
            )
        LOOP
            l_drzave.append(JSON_OBJECT_T(x.izlaz));
        END LOOP;

    SELECT
      count(1)
    INTO
       l_count
    FROM 
       drzave
    where
        ID = nvl(l_id, ID) ;

    l_obj.put('count',l_count);
    l_obj.put('data',l_drzave);
    out_json := l_obj;
 EXCEPTION
   WHEN OTHERS THEN
       --common.p_errlog('p_get_drzave', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
       l_obj.put('h_message', 'Gre�ka u obradi podataka');
       l_obj.put('h_errcode', 99);
       ROLLBACK;    


  end p_get_drzave;
  
  ------------------------------------------------------------------------------------
--p_get_pozivni_brojevi
create or replace NONEDITIONABLE PACKAGE BODY DOHVAT AS
e_iznimka exception;

procedure p_get_pozivni_brojevi(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
  l_obj JSON_OBJECT_T;
  l_pozivni_brojevi json_array_t :=JSON_ARRAY_T('[]');
  l_count number;
  l_id number;
  l_string varchar2(1000);
  l_search varchar2(100);
  l_page number; 
  l_perpage number; 

  BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.search'),
        JSON_VALUE(l_string, '$.page' ),
        JSON_VALUE(l_string, '$.perpage' )
    INTO
        l_id,
        l_search,
        l_page,
        l_perpage
    FROM 
       dual;

    FOR x IN (
            SELECT 
               json_object('ID' VALUE ID, 
                           'BROJ' VALUE BROJ,
                           'CREATED_AT' VALUE CREATED_AT,
                           'UPDATED_AT' VALUE UPDATED_AT) as izlaz
             FROM
                pozivni_brojevi
             where
                ID = nvl(l_id, ID) 
            )
        LOOP
            l_pozivni_brojevi.append(JSON_OBJECT_T(x.izlaz));
        END LOOP;

    SELECT
      count(1)
    INTO
       l_count
    FROM 
       pozivni_brojevi
    where
        ID = nvl(l_id, ID) ;

    l_obj.put('count',l_count);
    l_obj.put('data',l_pozivni_brojevi);
    out_json := l_obj;
 EXCEPTION
   WHEN OTHERS THEN
       --common.p_errlog('p_get_pozivni_brojevi', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
       l_obj.put('h_message', 'Gre�ka u obradi podataka');
       l_obj.put('h_errcode', 99);
       ROLLBACK;    


  end p_get_pozivni_brojevi;


  ------------------------------------------------------------------------------------
--p_get_vozila
create or replace NONEDITIONABLE PACKAGE BODY DOHVAT AS
e_iznimka exception;


procedure p_get_vozila(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
  l_obj JSON_OBJECT_T;
  l_vozila json_array_t :=JSON_ARRAY_T('[]');
  l_count number;
  l_id number;
  l_string varchar2(1000);
  l_search varchar2(100);
  l_page number; 
  l_perpage number; 

  BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.search'),
        JSON_VALUE(l_string, '$.page' ),
        JSON_VALUE(l_string, '$.perpage' )
    INTO
        l_id,
        l_search,
        l_page,
        l_perpage
    FROM 
       dual;

    FOR x IN (
            SELECT 
               json_object('ID' VALUE ID, 
                           'KORISNIK_ID' VALUE KORISNIK_ID,
                           'MODEL' VALUE MODEL,
                           'PROBLEM' VALUE PROBLEM,
                           'CREATED_AT' VALUE CREATED_AT,
                           'UPDATED_AT' VALUE UPDATED_AT) as izlaz
             FROM
                vozila
             where
                ID = nvl(l_id, ID) 
            )
        LOOP
            l_vozila.append(JSON_OBJECT_T(x.izlaz));
        END LOOP;

    SELECT
      count(1)
    INTO
       l_count
    FROM 
       vozila
    where
        ID = nvl(l_id, ID) ;

    l_obj.put('count',l_count);
    l_obj.put('data',l_vozila);
    out_json := l_obj;
 EXCEPTION
   WHEN OTHERS THEN
       --common.p_errlog('p_get_vozila', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
       l_obj.put('h_message', 'Gre�ka u obradi podataka');
       l_obj.put('h_errcode', 99);
       ROLLBACK;    


  end p_get_vozila;
  
    ------------------------------------------------------------------------------------
--p_get_korisnici

create or replace NONEDITIONABLE PACKAGE BODY DOHVAT AS
e_iznimka exception;


procedure p_get_korisnici(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
  l_obj JSON_OBJECT_T;
  l_korisnici json_array_t :=JSON_ARRAY_T('[]');
  l_count number;
  l_id number;
  l_string varchar2(1000);
  l_search varchar2(100);
  l_page number; 
  l_perpage number; 
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.search'),
        JSON_VALUE(l_string, '$.page' ),
        JSON_VALUE(l_string, '$.perpage' )
    INTO
        l_id,
        l_search,
        l_page,
        l_perpage
    FROM 
       dual;

    FOR x IN (
            SELECT 
               json_object('ID' VALUE ID, 
                           'IME' VALUE IME,
                           'PREZIME' VALUE PREZIME,
                           'POZIVNI_ID' VALUE OIB,
                           'EMAIL' VALUE EMAIL,
                           'MOBILNI_BROJ' VALUE SPOL,
                           'DRZAVA_ID' VALUE OVLASTI,
                           'CREATED_AT' VALUE CREATED_AT,
                           'UPDATED_AT' VALUE UPDATED_AT) as izlaz
             FROM
                korisnici
             where
                ID = nvl(l_id, ID) 
            )
        LOOP
            l_korisnici.append(JSON_OBJECT_T(x.izlaz));
        END LOOP;

    SELECT
      count(1)
    INTO
       l_count
    FROM 
       korisnici
    where
        ID = nvl(l_id, ID) ;

    l_obj.put('count',l_count);
    l_obj.put('data',l_korisnici);
    out_json := l_obj;
 EXCEPTION
   WHEN OTHERS THEN
       --common.p_errlog('p_get_korisnici', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
       l_obj.put('h_message', 'Gre�ka u obradi podataka');
       l_obj.put('h_errcode', 99);
       ROLLBACK;    

  END p_get_korisnici;