create or replace NONEDITIONABLE PACKAGE PODACI AS 

 procedure p_save_drzave(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
 procedure p_save_pozivni_brojevi(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
 procedure p_save_vozila(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
 procedure p_save_korisnici(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);



END PODACI;