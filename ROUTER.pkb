create or replace NONEDITIONABLE PACKAGE BODY ROUTER AS

  procedure p_main(p_in in varchar2, p_out out varchar2) AS
    l_obj JSON_OBJECT_T;
    l_procedura varchar2(40);
  BEGIN
    l_obj := JSON_OBJECT_T(p_in);

    SELECT
        JSON_VALUE(p_in, '$.procedura' RETURNING VARCHAR2)
    INTO
        l_procedura
    FROM DUAL;

    CASE l_procedura
    WHEN 'p_login' THEN
        dohvat.p_login(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_get_drzave' THEN
        dohvat.p_get_drzave(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_get_pozivni_brojevi' THEN
        dohvat.p_get_pozivni_brojevi(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_get_vozila' THEN
        dohvat.p_get_vozila(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_get_korisnici' THEN
        dohvat.p_get_korisnici(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_save_pozivni_brojevi' THEN
        podaci.p_save_pozivni_brojevi(JSON_OBJECT_T(p_in), l_obj);  
    WHEN 'p_save_vozila' THEN
        podaci.p_save_vozila(JSON_OBJECT_T(p_in), l_obj);  
    WHEN 'p_save_drzave' THEN
        podaci.p_save_drzave(JSON_OBJECT_T(p_in), l_obj);  
    WHEN 'p_save_korisnici' THEN
        podaci.p_save_korisnici(JSON_OBJECT_T(p_in), l_obj);    
    ELSE
        l_obj.put('h_message', ' Nepoznata metoda ' || l_procedura);
        l_obj.put('h_errcode', 997);
    END CASE;
    p_out := l_obj.TO_STRING;
  END p_main;
END ROUTER;