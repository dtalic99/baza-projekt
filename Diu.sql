--Unos u tablicu pozivni brojevi
INSERT INTO pozivni_brojevi (broj) values (095);
INSERT INTO pozivni_brojevi (broj) values (091);
INSERT INTO pozivni_brojevi (broj) values (092);
INSERT INTO pozivni_brojevi (broj) values (093);
INSERT INTO pozivni_brojevi (broj) values (098);
INSERT INTO pozivni_brojevi (broj) values (099);
INSERT INTO pozivni_brojevi (broj) values (097);
INSERT INTO pozivni_brojevi (broj) values (115);
INSERT INTO pozivni_brojevi (broj) values (619);
INSERT INTO pozivni_brojevi (broj) values (195);
INSERT INTO pozivni_brojevi (broj) values (085);
INSERT INTO pozivni_brojevi (broj) values (007);

--Unost u tablicu Drzave
INSERT INTO drzave (naziv) values ('Hrvatska');
INSERT INTO drzave (naziv) values ('Bosna i Hercegovina');
INSERT INTO drzave (naziv) values ('Crna Gora');
INSERT INTO drzave (naziv) values ('Makedonija');
INSERT INTO drzave (naziv) values ('Italija');
INSERT INTO drzave (naziv) values ('Zimbabve');
INSERT INTO drzave (naziv) values ('Kina');
INSERT INTO drzave (naziv) values ('Austrija');
INSERT INTO drzave (naziv) values ('Australija');
INSERT INTO drzave (naziv) values ('Njemacka');
INSERT INTO drzave (naziv) values ('Rusija');
INSERT INTO drzave (naziv) values ('Engleska');


--Unos u tablicu Korisnici
INSERT INTO korisnici(ime,prezime,dat_rod,pozivni_id,mobilni_broj,
email,password,drzava_id) values ('Domagoj','Talic','20/08/1999',1,1234567,'dtalic@gmail.com','talic123',1);
INSERT INTO korisnici(ime,prezime,dat_rod,pozivni_id,mobilni_broj,
email,password,drzava_id) values ('Pero','Peric','02/08/1987',2,1234566,'pericpero@gmail.com','pericp123',2);
INSERT INTO korisnici(ime,prezime,dat_rod,pozivni_id,mobilni_broj,
email,password,drzava_id) values ('Ivo','Ivic','27/12/1968',3,1234565,'iivic@gmail.com','iivic23',3);
INSERT INTO korisnici(ime,prezime,dat_rod,pozivni_id,mobilni_broj,
email,password,drzava_id) values ('Mirko','Maric','15/03/1989',4,1234564,'maricm@gmail.com','mirkom123',4);
INSERT INTO korisnici(ime,prezime,dat_rod,pozivni_id,mobilni_broj,
email,password,drzava_id) values ('Ivana','Tot','09/05/1992',5,1234563,'totivana@gmail.com','ivanatot123',5);
INSERT INTO korisnici(ime,prezime,dat_rod,pozivni_id,mobilni_broj,
email,password,drzava_id) values ('Snjezana','Hadnic','16/08/1977',6,1234562,'hladnics@gmail.com','snjezana123',6);
INSERT INTO korisnici(ime,prezime,dat_rod,pozivni_id,mobilni_broj,
email,password,drzava_id) values ('Igor','Skrtic','24/02/1959',7,1234561,'skrticigor@gmail.com','igors123',7);
INSERT INTO korisnici(ime,prezime,dat_rod,pozivni_id,mobilni_broj,
email,password,drzava_id) values ('ante','Sop','03/08/1988',8,1234560,'antesop@gmail.com','antesop123',8);
INSERT INTO korisnici(ime,prezime,dat_rod,pozivni_id,mobilni_broj,
email,password,drzava_id) values ('Gavaran','Kapetanovic','11/11/1955',9,1234577,'kapetanociv@gmail.com','kapetanovic123',9);
INSERT INTO korisnici(ime,prezime,dat_rod,pozivni_id,mobilni_broj,
email,password,drzava_id) values ('Daniel','Ricciardo','01/07/1989',10,1234557,'danielr@gmail.com','riccidan123',10);
INSERT INTO korisnici(ime,prezime,dat_rod,pozivni_id,mobilni_broj,
email,password,drzava_id) values ('Sebastian','Vettel','05/07/1987',11,1234547,'sebvettel@gmail.com','vettelseb23',11);
INSERT INTO korisnici(ime,prezime,dat_rod,pozivni_id,mobilni_broj,
email,password,drzava_id) values ('Zlatko','Crnic','18/04/1979',12,1234537,'zlatkoc@gmail.com','crniczlatko123',12);
INSERT INTO korisnici(ime,prezime,dat_rod,pozivni_id,mobilni_broj,
email,password,drzava_id) values ('Domagoj','Palic','20/08/1999',12,1234511,'dpalic@gmail.com','palic123',12);
INSERT INTO korisnici(ime,prezime,dat_rod,pozivni_id,mobilni_broj,
email,password,drzava_id) values ('Sasa','Peric','02/08/1987',12,1234522,'pericsasa@gmail.com','perics123',12);
INSERT INTO korisnici(ime,prezime,dat_rod,pozivni_id,mobilni_broj,
email,password,drzava_id) values ('Goran','Ivic','27/12/1968',12,1234533,'sivic@gmail.com','sivic23',12);
INSERT INTO korisnici(ime,prezime,dat_rod,pozivni_id,mobilni_broj,
email,password,drzava_id) values ('Mirko','Juric','15/03/1989',12,1234544,'mirkoj@gmail.com','mirkoj123',12);
INSERT INTO korisnici(ime,prezime,dat_rod,pozivni_id,mobilni_broj,
email,password,drzava_id) values ('Ivana','Stok','09/05/1992',12,1234555,'stokivana@gmail.com','ivanastok123',12);
INSERT INTO korisnici(ime,prezime,dat_rod,pozivni_id,mobilni_broj,
email,password,drzava_id) values ('Sara','Hadnic','16/08/1977',12,1234512,'hladnicsara@gmail.com','hladnicsara123',12);
INSERT INTO korisnici(ime,prezime,dat_rod,pozivni_id,mobilni_broj,
email,password,drzava_id) values ('Igor','krtic','24/02/1959',12,1234561,'krticigor@gmail.com','igors123',12);
INSERT INTO korisnici(ime,prezime,dat_rod,pozivni_id,mobilni_broj,
email,password,drzava_id) values ('ante','top','03/08/1988',12,1234560,'antetop@gmail.com','antetop123',12);

--Unos u tablicu vozila
INSERT INTO vozila (korisnik_id,model,problem) values(1,'audi','bosch pumpa');
INSERT INTO vozila (korisnik_id,model,problem) values(2,'mercedes','nerade svijetla');
INSERT INTO vozila (korisnik_id,model,problem) values(3,'golf','slabo kvacilo');
INSERT INTO vozila (korisnik_id,model,problem) values(4,'opel','razbijen retrovizor');
INSERT INTO vozila (korisnik_id,model,problem) values(5,'bmw','puknuti amortizeri');
INSERT INTO vozila (korisnik_id,model,problem) values(6,'ford','slabe kocnice');
INSERT INTO vozila (korisnik_id,model,problem) values(7,'chevrolet','pregirjavanje motora');
INSERT INTO vozila (korisnik_id,model,problem) values(8,'seat','neradi klima');
INSERT INTO vozila (korisnik_id,model,problem) values(9,'suzuki','nerade brisaci');
INSERT INTO vozila (korisnik_id,model,problem) values(10,'tesla','puknuta guma');
INSERT INTO vozila (korisnik_id,model,problem) values(11,'honda','nece uoaliti');
INSERT INTO vozila (korisnik_id,model,problem) values(12,'ferarri','zamjena kvacila');
INSERT INTO vozila (korisnik_id,model,problem) values(13,'hyundai','promjena filtera kabine');
INSERT INTO vozila (korisnik_id,model,problem) values(14,'renault','slaba rucna');
INSERT INTO vozila (korisnik_id,model,problem) values(15,'ferarri','zamjena haube');

--Update naredbi
UPDATE korisnici set ime = 'Florijan' where id = 4;
UPDATE korisnici set dat_rod = '01/01/1980' where id = 26;
UPDATE korisnici set ime = 'Ante', mobilni_broj = 02345879 where id = 27;
UPDATE drzave set naziv = 'Spanjolska' where id = 12;
UPDATE drzave set naziv = 'Brazil' where id = 2;
UPDATE drzave set naziv = 'Cile' where id = 9;
UPDATE pozivni_brojevi set broj = 539 where id = 7;
UPDATE pozivni_brojevi set broj = 444 where id = 11;
UPDATE pozivni_brojevi set broj = 871 where id = 1;
UPDATE vozila set model = 'lamborghini' where id = 29;
UPDATE vozila set model = 'volvo', problem='mali servis' where id = 20;
UPDATE vozila set model = 'tata' where id = 22;
UPDATE vozila set model = 'skoda', problem='slupana vrata' where id = 21;
UPDATE vozila set model = 'mazda', problem='zamjena ulja' where id = 26;
UPDATE korisnici set prezime = 'Grcic', email = 'grcic@gmail.com' where id = 27;
UPDATE korisnici set password = 'micamaca0011', email = 'slavko@gmail.com' where id = 29;

--Delete
DELETE FROM korisnici WHERE ID = 29;
DELETE FROM korisnici WHERE ID = 28;
DELETE FROM korisnici WHERE ID = 27;
DELETE FROM korisnici WHERE ID = 22;
DELETE FROM korisnici WHERE ID = 23;
DELETE FROM korisnici WHERE ID = 24;
DELETE FROM korisnici WHERE ID = 25;
DELETE FROM korisnici WHERE ID = 26;
DELETE FROM vozila WHERE ID = 14;
DELETE FROM vozila WHERE ID = 13;
DELETE FROM vozila WHERE ID = 15;
