create or replace NONEDITIONABLE PACKAGE DOHVAT AS 

  procedure p_login(in_json in json_object_t, out_json out json_object_t);
  procedure p_get_drzave(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_get_pozivni_brojevi(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_get_vozila(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_get_korisnici(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);


END DOHVAT;