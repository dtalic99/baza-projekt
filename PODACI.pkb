create or replace NONEDITIONABLE PACKAGE BODY PODACI AS
e_iznimka exception;
--save_drzave
-----------------------------------------------------------------------------------------
procedure p_save_drzave(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
      l_obj JSON_OBJECT_T;
      l_drzave drzave%rowtype;
      l_count number;
      l_id number;
      l_string varchar2(1000);
      l_search varchar2(100);
      l_page number; 
      l_perpage number;
      l_action varchar2(10);
  begin

     l_obj := JSON_OBJECT_T(in_json);  
     l_string := in_json.TO_STRING;

     SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.NAZIV'),
        JSON_VALUE(l_string, '$.CREATED_AT'),
        JSON_VALUE(l_string, '$.UPDATED_AT'),
        JSON_VALUE(l_string, '$.ACTION')
    INTO
        l_drzave.id,
        l_drzave.NAZIV,
        l_drzave.CREATED_AT,
        l_drzave.UPDATED_AT,
        l_action
    FROM 
       dual; 

    --FE kontrole
    if (nvl(l_action, ' ') = ' ') then
        if (filter.f_check_drzave(l_obj, out_json)) then
           raise e_iznimka; 
        end if;  
    end if;

    if (l_drzave.id is null) then
        begin
           insert into drzave(naziv) values
             (l_drzave.NAZIV);
           commit;

           l_obj.put('h_message', 'Uspje�no ste unijeli drzavu'); 
           l_obj.put('h_errcode', 0);
           out_json := l_obj;

        exception
           when others then 
              -- COMMON.p_errlog('p_users',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
               rollback;
               raise;
        end;
    else
       if (nvl(l_action, ' ') = 'delete') then
           begin
               delete drzave where id = l_drzave.id;
               commit;    

               l_obj.put('h_message', 'Uspje�no ste obrisali drzave'); 
               l_obj.put('h_errcode', 0);
               out_json := l_obj;
            exception
               when others then 
                   --COMMON.p_errlog('p_users',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
                   rollback;
                   raise;
            end;

       else

           begin
               update drzave
                  set NAZIV = l_drzave.NAZIV
               where
                  id = l_drzave.id;
               commit;    

               l_obj.put('h_message', 'Uspje�no ste promijenili naziv drzave'); 
               l_obj.put('h_errcode', 0);
               out_json := l_obj;
            exception
               when others then 
                   --COMMON.p_errlog('p_users',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
                   rollback;
                   raise;
            end;
       end if;     
    end if;


  exception
     when e_iznimka then
        out_json := l_obj; 
     when others then
        --COMMON.p_errlog('p_save_klijenti',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
        l_obj.put('h_message', 'Dogodila se gre�ka u obradi podataka!' || dbms_utility.format_error_backtrace || SQLERRM); 
        l_obj.put('h_errcode', 151);
        out_json := l_obj;
  END p_save_drzave;
  
  --save pozivni_brijevi
create or replace NONEDITIONABLE PACKAGE BODY PODACI AS
e_iznimka exception;

procedure p_save_pozivni_brojevi(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
      l_obj JSON_OBJECT_T;
      l_pozivni_brojevi pozivni_brojevi%rowtype;
      l_count number;
      l_id number;
      l_string varchar2(1000);
      l_search varchar2(100);
      l_page number; 
      l_perpage number;
      l_action varchar2(10);
  begin

     l_obj := JSON_OBJECT_T(in_json);  
     l_string := in_json.TO_STRING;

     SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.BROJ'),
        JSON_VALUE(l_string, '$.CREATED_AT'),
        JSON_VALUE(l_string, '$.UPDATED_AT'),
        JSON_VALUE(l_string, '$.ACTION')
    INTO
        l_pozivni_brojevi.id,
        l_pozivni_brojevi.BROJ,
        l_pozivni_brojevi.CREATED_AT,
        l_pozivni_brojevi.UPDATED_AT,
        l_action
    FROM 
       dual; 

    --FE kontrole
    if (nvl(l_action, ' ') = ' ') then
        if (filter.f_check_pozivni_brojevi(l_obj, out_json)) then
           raise e_iznimka; 
        end if;  
    end if;

    if (l_pozivni_brojevi.id is null) then
        begin
           insert into pozivni_brojevi(BROJ) values
             (l_pozivni_brojevi.BROJ);
           commit;

           l_obj.put('h_message', 'Uspje�no ste unijeli drzavu'); 
           l_obj.put('h_errcode', 0);
           out_json := l_obj;

        exception
           when others then 
              -- COMMON.p_errlog('p_users',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
               rollback;
               raise;
        end;
    else
       if (nvl(l_action, ' ') = 'delete') then
           begin
               delete pozivni_brojevi where id = l_pozivni_brojevi.id;
               commit;    

               l_obj.put('h_message', 'Uspje�no ste obrisali pozivni_brojevi'); 
               l_obj.put('h_errcode', 0);
               out_json := l_obj;
            exception
               when others then 
                   --COMMON.p_errlog('p_users',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
                   rollback;
                   raise;
            end;

       else

           begin
               update pozivni_brojevi
                  set BROJ = l_pozivni_brojevi.BROJ
               where
                  id = l_pozivni_brojevi.id;
               commit;    

               l_obj.put('h_message', 'Uspje�no ste promijenili BROJ pozivni_brojevi'); 
               l_obj.put('h_errcode', 0);
               out_json := l_obj;
            exception
               when others then 
                   --COMMON.p_errlog('p_users',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
                   rollback;
                   raise;
            end;
       end if;     
    end if;


  exception
     when e_iznimka then
        out_json := l_obj; 
     when others then
        --COMMON.p_errlog('p_save_klijenti',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
        l_obj.put('h_message', 'Dogodila se gre�ka u obradi podataka!' || dbms_utility.format_error_backtrace || SQLERRM); 
        l_obj.put('h_errcode', 151);
        out_json := l_obj;
  END p_save_pozivni_brojevi;
  
  --save vozila
create or replace NONEDITIONABLE PACKAGE BODY PODACI AS
e_iznimka exception;
  
  procedure p_save_vozila(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
      l_obj JSON_OBJECT_T;
      l_vozila vozila%rowtype;
      l_count number;
      l_id number;
      l_string varchar2(1000);
      l_search varchar2(100);
      l_page number; 
      l_perpage number;
      l_action varchar2(10);
  begin

     l_obj := JSON_OBJECT_T(in_json);  
     l_string := in_json.TO_STRING;

     SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.KORISNIK_ID),
        JSON_VALUE(l_string, '$.MODEL'),
        JSON_VALUE(l_string, '$.PROBLEM'),
        JSON_VALUE(l_string, '$.CREATED_AT'),
        JSON_VALUE(l_string, '$.UPDATED_AT'),
        JSON_VALUE(l_string, '$.ACTION')
    INTO
        l_vozila.id,
        l_vozila.KORISNIK_ID,
        l_vozila.MODEL,
        l_vozila.PROBLEM,
        l_vozila.CREATED_AT,
        l_vozila.UPDATED_AT,
        l_action
    FROM 
       dual; 

    --FE kontrole
    if (nvl(l_action, ' ') = ' ') then
        if (filter.f_check_vozila(l_obj, out_json)) then
           raise e_iznimka; 
        end if;  
    end if;

    if (l_vozila.id is null) then
        begin
           insert into vozila(KORISNIK_ID,MODEL,PROBLEM) values
             (l_vozila.KORISNIK_ID, l_vozila.MODEL,l_vozila.PROBLEM);
           commit;

           l_obj.put('h_message', 'Uspje�no ste unijeli vozilo'); 
           l_obj.put('h_errcode', 0);
           out_json := l_obj;

        exception
           when others then 
              -- COMMON.p_errlog('p_users',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
               rollback;
               raise;
        end;
    else
       if (nvl(l_action, ' ') = 'delete') then
           begin
               delete vozila where id = l_vozila.id;
               commit;    

               l_obj.put('h_message', 'Uspje�no ste obrisali vozila'); 
               l_obj.put('h_errcode', 0);
               out_json := l_obj;
            exception
               when others then 
                   --COMMON.p_errlog('p_users',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
                   rollback;
                   raise;
            end;

       else

           begin
               update vozila
                  set KORISNIK_ID = l_vozila.KORISNIK_ID,
                  MODEL = l_vozila.MODEL,
                  PROBLEM = l_vozila.PROBLEM,
               where
                  id = l_vozila.id;
               commit;    

               l_obj.put('h_message', 'Uspje�no ste promijenili vozilo'); 
               l_obj.put('h_errcode', 0);
               out_json := l_obj;
            exception
               when others then 
                   --COMMON.p_errlog('p_users',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
                   rollback;
                   raise;
            end;
       end if;     
    end if;


  exception
     when e_iznimka then
        out_json := l_obj; 
     when others then
        --COMMON.p_errlog('p_save_vozila',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
        l_obj.put('h_message', 'Dogodila se gre�ka u obradi podataka!' || dbms_utility.format_error_backtrace || SQLERRM); 
        l_obj.put('h_errcode', 151);
        out_json := l_obj;
  END p_save_vozila;
  
  --save korisnici
create or replace NONEDITIONABLE PACKAGE BODY PODACI AS
e_iznimka exception;
  
  procedure p_save_korisnici(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
      l_obj JSON_OBJECT_T;
      l_korisnici korisnici%rowtype;
      l_count number;
      l_id number;
      l_string varchar2(1000);
      l_search varchar2(100);
      l_page number; 
      l_perpage number;
      l_action varchar2(10);
  begin

     l_obj := JSON_OBJECT_T(in_json);  
     l_string := in_json.TO_STRING;

     SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.IME'),
        JSON_VALUE(l_string, '$.PREZIME' ),
        JSON_VALUE(l_string, '$.POZIVNI_ID' ),
        JSON_VALUE(l_string, '$.EMAIL' ),
        JSON_VALUE(l_string, '$.PASSWORD' ),
        JSON_VALUE(l_string, '$.MOBILNI_BROJ' ),
        JSON_VALUE(l_string, '$.DRZAVA_ID' ),
        JSON_VALUE(l_string, '$.CREATED_AT' ),
        JSON_VALUE(l_string, '$.UPDATED_AT' ),
        JSON_VALUE(l_string, '$.ACTION' )
    INTO
        l_korisnici.id,
        l_korisnici.IME,
        l_korisnici.PREZIME,
        l_korisnici.POZIVNI_ID,
        l_korisnici.EMAIL,
        l_korisnici.PASSWORD,
        l_korisnici.MOBILNI_BROJ,
        l_korisnici.DRZAVA_ID,
        l_korisnici.CREATED_AT,
        l_korisnici.UPDATED_AT,
        l_action
    FROM 
       dual; 

    --FE kontrole
    if (nvl(l_action, ' ') = ' ') then
        if (filter.f_check_korisnici(l_obj, out_json)) then
           raise e_iznimka; 
        end if;  
    end if;

    if (l_korisnici.id is null) then
        begin
           insert into korisnici (IME, PREZIME, POZIVNI_ID, EMAIL, PASSWORD, MOBILNI_BROJ, DRZAVA_ID) values
             (l_korisnici.IME, l_korisnici.PREZIME,
              l_korisnici.POZIVNI_ID, l_korisnici.EMAIL, l_korisnici.PASSWORD,
              l_korisnici.MOBILNI_BROJ, l_korisnici.DRZAVA_ID);
           commit;

           l_obj.put('h_message', 'Uspje�no ste unijeli korisnika'); 
           l_obj.put('h_errcode', 0);
           out_json := l_obj;

        exception
           when others then 
              -- COMMON.p_errlog('p_users',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
               rollback;
               raise;
        end;
    else
       if (nvl(l_action, ' ') = 'delete') then
           begin
               delete korisnici where id = l_korisnici.id;
               commit;    

               l_obj.put('h_message', 'Uspje�no ste obrisali korisnika'); 
               l_obj.put('h_errcode', 0);
               out_json := l_obj;
            exception
               when others then 
                   --COMMON.p_errlog('p_users',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
                   rollback;
                   raise;
            end;

       else

           begin
               update korisnici 
                  set IME = l_korisnici.IME,
                      PREZIME = l_korisnici.PREZIME,
                      POZIVNI_ID = l_korisnici.POZIVNI_ID,
                      EMAIL = l_korisnici.EMAIL,
                      MOBILNI_BOJ = l_korisnici.MOBILNI_BOJ,
                      DRZAVA_ID = l_korisnici.DRZAVA_ID
               where
                  id = l_korisnici.id;
               commit;    

               l_obj.put('h_message', 'Uspje�no ste promijenili korsnika'); 
               l_obj.put('h_errcode', 0);
               out_json := l_obj;
            exception
               when others then 
                   --COMMON.p_errlog('p_users',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
                   rollback;
                   raise;
            end;
       end if;     
    end if;


  exception
     when e_iznimka then
        out_json := l_obj; 
     when others then
        --COMMON.p_errlog('p_save_korisnici',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
        l_obj.put('h_message', 'Dogodila se gre�ka u obradi podataka!'); 
        l_obj.put('h_errcode', 151);
        out_json := l_obj;
  END p_save_korisnici;

  