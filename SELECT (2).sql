select 
    concat(k.ime || ' ', k.prezime) as "Korinik",
    d.naziv as "Drzava porijekla"   
from
    korisnici k,
    drzave d
where 
    k.drzava_id = d.id;
    
select 
    concat(k.ime || ' ', k.prezime) as "Vlasnik",
    v.model as "Automobil"   
from
    korisnici k,
    vozila v
where 
    v.korisnik_id = k.id;
    
select
    d.naziv,
    p.broj as "Pozivni broj"
from
    drzave d,
    pozivni_brojevi p,
    korisnici k
where
    k.drzava_id = d.id AND
    k.pozivni_id = p.id;
    

select 
    concat(k.ime || ' ', k.prezime) as "Vlasnik",
    v.problem as "Kvar"   
from
    korisnici k,
    vozila v
where 
    v.korisnik_id = k.id;
    
    
select 
    concat(k.ime || ' ', k.prezime) as "Vlasnik",
    concat(p.broj, k.mobilni_broj) as "Broj vlasnika"   
from
    korisnici k,
    pozivni_brojevi p
where 
    k.pozivni_id = p.id;

select
    ime,
    prezime
from korisnici;
    
    
select naziv from drzave;

select model, problem from vozila;

select * from korisnici;

select * from vozila;