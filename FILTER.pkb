
  function f_check_kavane(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) return boolean AS
      l_obj JSON_OBJECT_T;
      l_kavane kavane%rowtype;
      l_count number;
      l_id number;
      l_string varchar2(1000);
      l_search varchar2(100);
      l_page number; 
      l_perpage number;
  BEGIN
     l_obj := JSON_OBJECT_T(in_json);  
     l_string := in_json.TO_STRING;

     SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.NAZIV'),
        JSON_VALUE(l_string, '$.MJESTO' ),
        JSON_VALUE(l_string, '$.IDVLASNIK' ),
        JSON_VALUE(l_string, '$.LAT' ),
        JSON_VALUE(l_string, '$.LNG' ),
        JSON_VALUE(l_string, '$.RADNOVRIJEME' )

    INTO
        l_kavane.id,
        l_kavane.NAZIV,
        l_kavane.MJESTO,
        l_kavane.IDVLASNIK,
        l_kavane.LAT,
        l_kavane.LNG,
        l_kavane.RADNOVRIJEME
    FROM 
       dual; 

    if (nvl(l_kavane.NAZIV, 0) = 0) then   
       l_obj.put('h_message', 'Molimo unesite Naziv kavane'); 
       l_obj.put('h_errcode', 103);
       raise e_iznimka;
    end if;

    if (nvl(l_kavane.MJESTO, ' ') = ' ') then   
       l_obj.put('h_message', 'Molimo unesite mjesto kavane'); 
       l_obj.put('h_errcode', 104);
       raise e_iznimka;
    end if;
    
    if (nvl(l_kavane.IDVLASNIK, ' ') = ' ') then   
       l_obj.put('h_message', 'Molimo unesite Vlasnika kavane(IDVlasnik)'); 
       l_obj.put('h_errcode', 105);
       raise e_iznimka;
    end if;
    
        if (nvl(l_kavane.LAT, ' ') = ' ') then   
       l_obj.put('h_message', 'Molimo unesite Lat kavane'); 
       l_obj.put('h_errcode', 106);
       raise e_iznimka;
    end if;
    
        if (nvl(l_kavane.LNG, ' ') = ' ') then   
       l_obj.put('h_message', 'Molimo unesite Lng kavane'); 
       l_obj.put('h_errcode', 107);
       raise e_iznimka;
    end if;
    
        if (nvl(l_kavane.RADNOVRIJEME, ' ') = ' ') then   
       l_obj.put('h_message', 'Molimo unesite Radno vrijeme kavane'); 
       l_obj.put('h_errcode', 108);
       raise e_iznimka;
    end if;



    out_json := l_obj;
    return false;

  EXCEPTION
     WHEN E_IZNIMKA THEN
        return true;
     WHEN OTHERS THEN
        --COMMON.p_errlog('p_check_kavane',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
        l_obj.put('h_message', 'Dogodila se greška u obradi podataka!'); 
        l_obj.put('h_errcode', 109);
        out_json := l_obj;
        return true;
  END f_check_kavane;