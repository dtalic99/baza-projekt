drop table vozila;
drop table korisnici;
drop table drzave;
drop table pozivni_brojevi;
drop sequence VOZILA_ID_SEQ;
drop sequence KORISNICI_ID_SEQ;
drop sequence DRZAVE_ID_SEQ;
drop sequence POZIVNI_BROJEVI_ID_SEQ;
/
CREATE TABLE korisnici (
	id INT NOT NULL,
	ime VARCHAR2(40) NOT NULL,
	prezime VARCHAR2(40) NOT NULL,
	dat_rod VARCHAR2(11) NOT NULL,
	pozivni_id INT,
	mobilni_broj NUMBER(7, 0) NOT NULL,
	drzava_id INT NOT NULL,
    	email VARCHAR2(40) NOT NULL,
	password VARCHAR2(40) NOT NULL,
	created_at TIMESTAMP NOT NULL,
	updated_at TIMESTAMP NOT NULL,
	constraint KORISNICI_PK PRIMARY KEY (id));

CREATE sequence KORISNICI_ID_SEQ;

CREATE trigger BI_KORISNICI_ID
  before insert on korisnici
  for each row
begin
  select KORISNICI_ID_SEQ.nextval into :NEW.id from dual;
end;
/
CREATE OR REPLACE TRIGGER BI_KORISNICI 
    BEFORE INSERT ON korisnici
    for each row
BEGIN
  SELECT 
    SYSTIMESTAMP
  INTO 
    :NEW.updated_at
  FROM DUAL;
  
  SELECT 
    SYSTIMESTAMP
  INTO 
    :NEW.created_at
  FROM DUAL;
END;

/
CREATE TABLE pozivni_brojevi (
	id INT NOT NULL,
	broj VARCHAR2(3) NOT NULL,
	created_at TIMESTAMP NOT NULL,
	updated_at TIMESTAMP NOT NULL,
	constraint POZIVNI_BROJEVI_PK PRIMARY KEY (id));

CREATE sequence POZIVNI_BROJEVI_ID_SEQ;

CREATE trigger BI_POZIVNI_BROJEVI_ID
  before insert on pozivni_brojevi
  for each row
begin
  select POZIVNI_BROJEVI_ID_SEQ.nextval into :NEW.id from dual;
end;
/
CREATE OR REPLACE TRIGGER BI_POZIVNI_BROJEVI 
    BEFORE INSERT ON pozivni_brojevi
    for each row
BEGIN
  SELECT 
    SYSTIMESTAMP
  INTO 
    :NEW.updated_at
  FROM DUAL;
  
  SELECT 
    SYSTIMESTAMP
  INTO 
    :NEW.created_at
  FROM DUAL;
END;

/
CREATE TABLE drzave (
	id INT NOT NULL,
	naziv VARCHAR2(255) NOT NULL,
	created_at TIMESTAMP NOT NULL,
	updated_at TIMESTAMP NOT NULL,
	constraint DRZAVE_PK PRIMARY KEY (id));

CREATE sequence DRZAVE_ID_SEQ;

CREATE trigger BI_DRZAVE_ID
  before insert on drzave
  for each row
begin
  select DRZAVE_ID_SEQ.nextval into :NEW.id from dual;
end;
/
CREATE OR REPLACE TRIGGER BI_DRZAVE 
    BEFORE INSERT ON drzave
    for each row
BEGIN
  SELECT 
    SYSTIMESTAMP
  INTO 
    :NEW.updated_at
  FROM DUAL;
  
  SELECT 
    SYSTIMESTAMP
  INTO 
    :NEW.created_at
  FROM DUAL;
END;

/
CREATE TABLE vozila (
	id INT NOT NULL,
	korisnik_id INT NOT NULL,
	model VARCHAR2(40) NOT NULL,
	problem VARCHAR2(500) NOT NULL,
	created_at TIMESTAMP NOT NULL,
	updated_at TIMESTAMP NOT NULL,
	constraint VOZILA_PK PRIMARY KEY (id));

CREATE sequence VOZILA_ID_SEQ;

CREATE trigger BI_VOZILA_ID
  before insert on vozila
  for each row
begin
  select VOZILA_ID_SEQ.nextval into :NEW.id from dual;
end;
/
CREATE OR REPLACE TRIGGER BI_VOZILA 
    BEFORE INSERT ON vozila
    for each row
BEGIN
  SELECT 
    SYSTIMESTAMP
  INTO 
    :NEW.updated_at
  FROM DUAL;
  
  SELECT 
    SYSTIMESTAMP
  INTO 
    :NEW.created_at
  FROM DUAL;
END;

/
ALTER TABLE korisnici ADD CONSTRAINT korisnici_fk0 FOREIGN KEY (pozivni_id) REFERENCES pozivni_brojevi(id) ON Delete CASCADE;
ALTER TABLE korisnici ADD CONSTRAINT korisnici_fk1 FOREIGN KEY (drzava_id) REFERENCES drzave(id)ON DELETE CASCADE;



ALTER TABLE vozila ADD CONSTRAINT vozila_fk0 FOREIGN KEY (korisnik_id) REFERENCES korisnici(id) ON DELETE CASCADE;
